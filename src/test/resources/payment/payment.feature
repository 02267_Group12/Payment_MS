Feature: Payment
	Scenario: SOAP Successful Payment
		Given the customer "Andrew" "Ryan" with CPR "061111-0089" has a bank account
		And the balance of that account is 1000
		And the customer is registered with DTUPay
		And the merchant "Frank" "Fontaine" with CPR number "730521-0041" has a bank account
		And the balance of that account is 2000
		And the merchant is registered with DTUPay
		When the merchant initiates a payment for 10 kr by the customer
		Then the payment is successful
		And the balance of the customer at the bank is 990 kr
		And the balance of the merchant at the bank is 2010 kr

	Scenario: SOAP Customer is not known
		Given the customer "Mister" "Atlas" with CPR "000100-0000" has not a bank account

	Scenario: REST Successful Payment
		Given the customer "Andrew" "Ryan" with CPR "061111-0089" has a bank account
		And the balance of that account is 1000
		And the customer is registered with DTUPay
		And the merchant "Frank" "Fontaine" with CPR number "730521-0041" has a bank account
		And the balance of that account is 2000
		And the merchant is registered with DTUPay
		When the merchant initiates a payment for 100 kr by the customer from the REST API
		Then the customer can retrieve the transaction from the REST API
