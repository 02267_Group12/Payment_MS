package org.acme;

import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.Transaction;
import entity.local.DTUTransaction;
import entity.local.MerchantTransaction;
import manager.SimpleDTUPay;
import messaging.entities.EVENT_TYPE;
import messaging.entities.Event;
import messaging.mq.MQSender;
import messaging.utils.JsonUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * Payment REST API
 * @author: Peter and Jordi
 */
@Path("/payments")
public class PaymentResource {

    private String generateID(){
        return UUID.randomUUID().toString();
    }
    private static final String SERVICE_NAME = "Payment and report";

    static SimpleDTUPay dtuPay = new SimpleDTUPay();
    private MQSender mqSender = new MQSender();

    private void consumeToken(String token_id){
        var event = new Event(EVENT_TYPE.TOKEN_USE, SERVICE_NAME, token_id, String.class.getName());
        mqSender.sendEvent(event, "token.use");
    }

    private void addTransactionToReport(DTUTransaction transaction){
        var event = new Event(EVENT_TYPE.NEW_TRANSACTION, SERVICE_NAME,
                JsonUtils.toJson(transaction, DTUTransaction.class), DTUTransaction.class.getName());
        mqSender.sendEvent(event, "payment.new");
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response initiateTransaction(MerchantTransaction transaction){
        // Generating the pending transaction ID
        String newTransaction_id = generateID();

        // Ask for all the information using async communication (MQ)
        dtuPay.createNewPendingTransaction(newTransaction_id,transaction.getMid(), transaction.getDescription(), transaction.getAmount());
        dtuPay.askForCIDFromToken(transaction.getTokenId(), newTransaction_id);
        dtuPay.askForAccountFromID(transaction.getMid(), newTransaction_id);

        return Response
                .ok(new GenericEntity<String>(newTransaction_id){})
                .build();
    }

    @POST
    @Path("/{transaction_id}")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response payToMerchant(@PathParam("transaction_id") String transaction_id, String tid)
    {
        try{
            // Apply the payment only if the pending transaction is ready
            var paymentData = dtuPay.getPaymentInformationFromID(transaction_id);
            var newDTUTransaction = dtuPay.getPendingTransaction(transaction_id);

            if(paymentData.getCreditor() == null || paymentData.getDebtor() == null)
                return Response.status(425, "Transaction not ready yet").build();

            // Consume the token
            consumeToken(tid);

            // Get the pending transaction information and execute the payment
            paymentData.setAmount(paymentData.getAmount());
            dtuPay.pay(paymentData);

            // Add the transaction to the report generation using MQ
            addTransactionToReport(newDTUTransaction);

            return Response.status(200, "Transaction is successful: "+transaction_id).build();
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @GET
    public String getResponse(){
        return "Success";
    }
}