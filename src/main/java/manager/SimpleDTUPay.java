package manager;

import dtu.rs.entities.DTUPayAccount;
import dtu.rs.entities.DTUPayAccountInfo;
import dtu.rs.entities.PaymentData;
import dtu.ws.fastmoney.*;
import entity.local.DTUTransaction;
import messaging.entities.EVENT_TYPE;
import messaging.entities.Event;
import messaging.entities.EventProcessor;
import messaging.mq.MQSender;
import messaging.utils.JsonUtils;

import java.math.BigDecimal;
import java.util.HashMap;

public class SimpleDTUPay implements EventProcessor {

    private static final String SERVICE_NAME = "Payment and report";
    private BankService bankService = new BankServiceService().getBankServicePort();
    private static HashMap<String, PaymentData> pendingTransactions = new HashMap<>();
    private static HashMap<String, DTUTransaction> newTransactionsReport = new HashMap<>();

    public void createBankAccount(String firstName, String lastName, String cpr, BigDecimal amount){
        User user = new User();
        user.setCprNumber(cpr);
        user.setFirstName(firstName);
        user.setLastName(lastName);

        try {
            bankService.createAccountWithBalance(user, amount);
        } catch (BankServiceException_Exception e) {
            //e.printStackTrace();
            System.out.println("Account already exists.");
        }
    }

    public void removeBankAccount(String id){
        try {
            bankService.retireAccount(id);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    public BigDecimal getAccountBalance(User user) {
        return getAccountFromActor(user).getBalance();
    }

    public Account getAccountFromActor(User user){
        try {
            return bankService.getAccountByCprNumber(user.getCprNumber());
        } catch (BankServiceException_Exception e) {
            return null;
        }
    }

    public Account getAccountFromCpr(String cpr){
        try {
            return bankService.getAccountByCprNumber(cpr);
        } catch (BankServiceException_Exception e) {
            return null;
        }
    }

    public boolean hasABankAccount(User user){
        return getAccountFromActor(user) != null;
    }

    public Transaction pay(PaymentData paymentData) throws BankServiceException_Exception {
        // BigDecimal amount, String actualCustomer, String actualMerchant
        Transaction transaction = new Transaction();
        transaction.setAmount(paymentData.getAmount());
        transaction.setCreditor(paymentData.getCreditor());
        transaction.setDebtor(paymentData.getDebtor());
        transaction.setDescription(paymentData.getDescription());

        bankService.transferMoneyFromTo(
                bankService.getAccountByCprNumber(paymentData.getDebtor()).getId(),
                bankService.getAccountByCprNumber(paymentData.getCreditor()).getId(),
                paymentData.getAmount(),
                paymentData.getDescription());

        return transaction;
    }

    public PaymentData getPaymentInformationFromID(String transaction_id){
        return pendingTransactions.get(transaction_id);
    }

    public DTUTransaction getPendingTransaction(String transaction_id){
        return newTransactionsReport.get(transaction_id);
    }

    public void createNewPendingTransaction(String id, String mid, String description, BigDecimal amount){
        var trans = new PaymentData();
        var dtutrans = new DTUTransaction();
        trans.setAmount(amount);
        trans.setDescription(description);
        dtutrans.setAmount(amount);
        dtutrans.setMid(mid);
        dtutrans.setDescription(description);
        pendingTransactions.put(id, trans);
        newTransactionsReport.put(id, dtutrans);
    }

    public void askForCIDFromToken(String token_id, String pendingTransaction_id){
        var event = new Event(EVENT_TYPE.TOKEN_GET_USER_ID, SERVICE_NAME, token_id, String.class.getName());
        event.setExtra(pendingTransaction_id);
        new MQSender().sendEvent(event, "token.get_cid");
    }

    public void askForAccountFromID(String user_id, String pendingTransaction_id){
        var event = new Event(EVENT_TYPE.GET_ACCOUNT, SERVICE_NAME, user_id, String.class.getName());
        event.setExtra(pendingTransaction_id);
        new MQSender().sendEvent(event, "account.get");
    }

    @Override
    public void processMessage(String owner, EVENT_TYPE type, Object content, String extra) throws Exception {
        // Get account from Customer or Merchant ID from MQ.
        // Extra: Pending transaction ID
        if(type == EVENT_TYPE.RESPONSE_GET_ACCOUNT_CUSTOMER || type == EVENT_TYPE.RESPONSE_GET_ACCOUNT_MERCHANT){
            DTUPayAccountInfo account = (DTUPayAccountInfo) content;

            var actualPendingTransaction = pendingTransactions.get(extra);
            var actualPendingDTUTransaction = newTransactionsReport.get(extra);
            PaymentData paymentData;
            DTUTransaction dtuTransaction;

            if(actualPendingTransaction == null || actualPendingDTUTransaction == null) {
                paymentData = new PaymentData();
                dtuTransaction = new DTUTransaction();
                System.out.println(extra + " pending transaction ID does not exist. Creating new...");
            }else{
                paymentData = actualPendingTransaction;
                dtuTransaction = actualPendingDTUTransaction;
            }

            if(type == EVENT_TYPE.RESPONSE_GET_ACCOUNT_CUSTOMER) {
                paymentData.setDebtor(account.getCprNumber());
                dtuTransaction.setCid(account.getId());
            }
            else {
                paymentData.setCreditor(account.getCprNumber());
                dtuTransaction.setMid(account.getId());
            }
            System.out.println("Adding new User for pending transaction "+extra+": "+account);
            pendingTransactions.put(extra, paymentData);
            newTransactionsReport.put(extra, dtuTransaction);
        }

        // Get the CID from the customer token and then ask for the customer account with MQ (to initiate a transaction).
        // Extra: Pending transaction ID
        else if(type == EVENT_TYPE.RESPONSE_TOKEN_GET_USER_ID){
            String cid = (String) content;
            askForAccountFromID(cid, extra);
        }
    }
}
