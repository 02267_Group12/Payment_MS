package messaging.mq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import messaging.entities.Event;
import messaging.utils.JsonUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MQSender {

    private static final String EXCHANGE_NAME = "topic_logs";
    private static final String HOSTNAME = "g-12.compute.dtu.dk";

    public MQSender(){}

    public void sendEvent(Event event, String routingKey){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOSTNAME);
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.exchangeDeclare(EXCHANGE_NAME, "topic");

            String message = JsonUtils.toJson(event, Event.class);

            channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + routingKey + "':'" + event + "'");

        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
