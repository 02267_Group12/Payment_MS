FROM openjdk:11

ARG JAR_FILE=Payment_Report_MS-1.0.0-SNAPSHOT-runner.jar
ARG EXTERNAL_PORT=8081
ENV JAR_FILE=$JAR_FILE

EXPOSE $EXTERNAL_PORT:8080
WORKDIR /usr/src/
COPY target/lib /usr/src/lib
COPY target/$JAR_FILE /usr/src

CMD java -jar $JAR_FILE